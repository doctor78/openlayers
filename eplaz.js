// Imports
var Map = ol.Map;
var View = ol.View;
var Point = ol.geom.Point;
var { Tile, Vector, WMTS } = ol.layer;
var TileLayer = Tile;
var VectorLayer = Vector;
var WMTSLayer = WMTS;
var { WMTS, OSM }  = ol.source;
var VectorSource = ol.source.Vector;
var Draw = ol.interaction.Draw;
var MousePosition = ol.control.MousePosition;
var createStringXY = ol.coordinate.createStringXY;
var { defaults, ZoomToExtent } = ol.control;
var defaultControls = defaults;
var WKT = ol.format.WKT;
var Projection = ol.proj.Projection;
var { register } = ol.proj.proj4;
var { Circle, Fill, Icon, Stroke, Style, Text } = ol.style;
var { getWidth, getTopLeft } = ol.extent;
var WMTSTileGrid = ol.tilegrid.WMTS

var  WMTSCapabilities = ol.format.WMTSCapabilities;
var { DEVICE_PIXEL_RATIO } = ol.has;
var {optionsFromCapabilities } = ol.source.WMTS;

// Some constants
var mapExtent = [372919.40, 31636.59, 622413.72, 193879.73];
var mapCenter = [499613.77, 112247.58];
var mapZoom = 2.3;

// Set up EPSG_3794 projection that is used for Slovenia
proj4.defs("EPSG:3794", "+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=500000 +y_0=-5000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
register(proj4);

var projection = new Projection({
    code: 'EPSG:3794',
    extent: mapExtent
});

// Define layer styles
var polygonsStyle = new Style({
    fill: new Fill({
        color: 'red'
    }),
    stroke: new Stroke({
        color: 'white'
    })
})

var pointsStyle = new Style({
    image: new Circle({
        radius: 5,
        fill: new Fill({
            color: 'green'
        }),
        stroke: new Stroke({
            color: [255, 0, 0],
            width: 1
        })
    })
})
var linesStyle = function (feature) {
    var geometry = feature.getGeometry();
    var styles = [
        // linestring
        new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'green',
                width: 2
            })
        })
    ];

    geometry.forEachSegment(function (start, end) {
        var dx = end[0] - start[0];
        var dy = end[1] - start[1];
        var rotation = Math.atan2(dy, dx);

        var lineStr1 = new ol.geom.LineString([end, [end[0] - 1000, end[1] + 1000]]);
        lineStr1.rotate(rotation, end);
        var lineStr2 = new ol.geom.LineString([end, [end[0] - 1000, end[1] - 1000]]);
        lineStr2.rotate(rotation, end);

        var stroke = new ol.style.Stroke({
            color: 'green',
            width: 1
        });

        styles.push(new ol.style.Style({
            geometry: lineStr1,
            stroke: stroke
        }));
        styles.push(new ol.style.Style({
            geometry: lineStr2,
            stroke: stroke
        }));
    });

    return styles;
};

// Initialize WKT formatter
var wkt = new WKT({
    splitCollection: true
});

// Define sources
var polygonsSource = new VectorSource({
    wrapX: false
});
var pointsSource = new VectorSource({
    wrapX: false
});
var linesSource = new VectorSource({
    wrapX: false
});

// Parse WKT geometries, reate feature and add them to sources
var polygonsWKT = $('#p_geom_WKT').val();
if (polygonsWKT != null || polygonsWKT != "") {

    var features = wkt.readFeatures(polygonsWKT, {
        dataProjection: projection,
        featureProjection: projection
    });

    polygonsSource.addFeatures(features);
}

var pointsWKT = $('#t_geom_WKT').val();
if (pointsWKT != null || pointsWKT != "") {

    var features = wkt.readFeatures(pointsWKT, {
        dataProjection: projection,
        featureProjection: projection
    });

    pointsSource.addFeatures(features);
}

var linesWKT = $('#l_geom_WKT').val();
if (linesWKT != null || linesWKT != "") {

    var features = wkt.readFeatures(linesWKT, {
        dataProjection: projection,
        featureProjection: projection
    });

    linesSource.addFeatures(features);
}

// Create layers
var osmLayer = new TileLayer({
    source: new OSM()
});


const projectionExtent = projection.getExtent();
const size = getWidth(projectionExtent) / 256;
const resolutions = new Array(14);
const matrixIds = new Array(14);
for (let z = 0; z < 14; ++z) {
    // generate resolutions and matrixIds arrays for this WMTS
    resolutions[z] = size / Math.pow(2, z);
    matrixIds[z] = "EPSG:3794:" + z;
}
var wmts = new TileLayer({
    opacity: 0.7,
    source: new WMTS({
        attributions: '...',
        url: 'https://prostor4.gov.si/ows2-gwc-pub/service/wmts',
        layer: 'SI.GURS.ZPDZ:DOF050',
        matrixSet: 'EPSG:3794',
        format: 'image/png',
        projection: projection,
        tileGrid: new WMTSTileGrid({
            origin: getTopLeft(projectionExtent),
            resolutions: resolutions,
            matrixIds: matrixIds
        })
    })
});

var polygonsLayer = new VectorLayer({
    source: polygonsSource,
    style: polygonsStyle,
    opacity: 0.5
});

var pointsLayer = new VectorLayer({
    source: pointsSource,
    style: pointsStyle,
});

var linesLayer = new VectorLayer({
    source: linesSource,
    style: linesStyle
});


var mousePositionControl = new MousePosition({
    coordinateFormat: createStringXY(4),
    projection: projection,
    // comment the following two lines to have the mouse position
    // be placed within the map.
    //className: 'custom-mouse-position',
    //target: document.getElementById('mouse-position'),
    undefinedHTML: '&nbsp;'
});

var zoomToExtentControl = new ZoomToExtent({
    extent: mapExtent
})

var map = new Map({
    controls: defaultControls().extend(
        [
            mousePositionControl,
            zoomToExtentControl
        ]),
    target: 'map',
    view: new View({
        projection: projection,
        center: mapCenter,
        zoom: mapZoom
    })
});

    map.addLayer(wmts);
map.addLayer(polygonsLayer);
map.addLayer(pointsLayer);
map.addLayer(linesLayer);

var draw; // global so we can remove it later
function addInteraction() {
    var type = typeSelect.value;

    switch (type) {
        case 'LineString':
            source = linesSource;
            break;
        case 'Point':
            source = pointsSource;
            break;
        case 'Polygon':
            source = polygonsSource;
            break;
        default:
            break;
    }

    if (type !== 'None') {
        draw = new Draw({
            source: source,
            type: typeSelect.value
        });
        map.addInteraction(draw);
    }
}


/**
 * Handle events.
 */
var typeSelect = document.getElementById('type');

typeSelect.onchange = function () {
    map.removeInteraction(draw);
    addInteraction();
};

addInteraction();

$("#polygons").change(function () {
    polygonsLayer.setVisible(!polygonsLayer.getVisible());
});

$("#points").change(function () {
    pointsLayer.setVisible(!pointsLayer.getVisible());
});

$("#lines").change(function () {
    linesLayer.setVisible(!linesLayer.getVisible());
});

polygonsSource.on('addfeature', function (evt) {
    var feature = evt.feature;
    var features = source.getFeatures();
    $('#p_geom_WKT').val(wkt.writeFeatures(features));
});

pointsSource.on('addfeature', function (evt) {
    var feature = evt.feature;
    var features = source.getFeatures();
    $('#t_geom_WKT').val(wkt.writeFeatures(features));
});

linesSource.on('addfeature', function (evt) {
    var feature = evt.feature;
    var features = source.getFeatures();
    $('#l_geom_WKT').val(wkt.writeFeatures(features));
});
